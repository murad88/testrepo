﻿namespace Boft.Shell.Model
{
    public interface IUserInfo
    {
        string Login { get; }

        string Fullname { get; }
    }
}