﻿using System;

namespace Boft.Shell.Model
{
    public interface IImageInfo
    {
        string PreviewUri { get; }

        string Uri { get; }

        DateTime Date { get; }

        string Description { get; }

        IUserInfo User { get; }

        string Location { get; }
    }
}