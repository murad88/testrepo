﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Boft.Shell.Model.FileSystem
{
    /// <summary>
    /// В данный момент реализация не требуется.
    /// </summary>
    internal class FileSystemImageProvider : IImageProvider
    {
        public async Task<IEnumerable<IImageInfo>> GetImages(int count)
        {
            throw new NotImplementedException();
        }

        public event EventHandler<ImagesRecievedEventArgs> ImagesRecieved;

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Start(DateTime start, DateTime end)
        {
            throw new NotImplementedException();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public void Pause()
        {
            throw new NotImplementedException();
        }

        public void Resume()
        {
            throw new NotImplementedException();
        }
    }
}