﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Boft.Shell.Model.Instagram
{
    public class OverLimitException : Exception
    {
        public OverLimitException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }

    public class AccessDeniedException : Exception
    {
        public AccessDeniedException() { }

        public AccessDeniedException(string message, Exception exception)
            : base(message, exception)
        {
        }
    }


    /// <summary>
    /// Интерфейс класса, осуществляющего взаимодействие с Instagram API.
    /// </summary>
    public interface IInstagramAPIClient
    {
        Task<bool> CheckPrivacy(string username, string token);

        Task<string> GrantAccess(string clientid, string username, string password);

        Task RevokeAccess(string clientid, string username, string password);

        Task<Response> GetUserImages(string userId, string accessToken, CancellationToken cancelationToken);

        Task<Response> GetUserImages(string userId, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp, CancellationToken cancelationToken);

        Task<Response> GetImagesByTag(string tag, string accessToken, CancellationToken cancelationToken);

        Task<Response> GetImagesByTag(string tag, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp, CancellationToken cancelationToken);

        Task FollowUser(string userId, string accessToken);

        Task<Response> GetInstagramImages(string url, CancellationToken cancelationToken);
    }
}