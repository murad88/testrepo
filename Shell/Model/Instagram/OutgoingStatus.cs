﻿using Newtonsoft.Json;

namespace Boft.Shell.Model.Instagram
{
    public enum OutgoingStatus
    {
        [JsonProperty("follows")]
        Follows,
        [JsonProperty("requested")]
        Requested,
        [JsonProperty("none")]
        None
    }
}