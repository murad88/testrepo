﻿using Boft.Shell.Helpers;

namespace Boft.Shell.Model.Instagram
{
    public sealed class TagImageProvider : InstagramImageProvider
    {
        public TagImageProvider(IInstagramAPIClient client, string accessToken, string tag)
            : base(client, accessToken)
        {
            Url = InstagramUrlHelper.GetTagUrl(tag);
        }
    }
}