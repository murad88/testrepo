﻿using Newtonsoft.Json;

namespace Boft.Shell.Model.Instagram
{
    internal class UserInfo : IUserInfo
    {
        [JsonProperty("username")]
        public string Login { get; set; }

        [JsonProperty("full_name")]
        public string Fullname { get; set; }

        [JsonProperty("profile_picture")]
        public string Image { get; set; }
    }
}