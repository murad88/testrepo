﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Boft.Shell.Helpers;
using Newtonsoft.Json;

namespace Boft.Shell.Model.Instagram
{
    public class InstagramAPIClient : IInstagramAPIClient
    {
        /// <summary>
        /// Используется для запросов к Instagram API
        /// </summary>
        private readonly HttpClient _httpClient = new HttpClient();

        /// <summary>
        /// В данный момент реализация не требуется.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<bool> CheckPrivacy(string username, string token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// В данный момент реализация не требуется.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<string> GrantAccess(string clientid, string username, string password)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// В данный момент реализация не требуется.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task RevokeAccess(string clientid, string username, string password)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Получение изображений по имени пользователя.
        /// </summary>
        /// <exception cref="AccessDeniedException">
        /// Выбрасывается в случае неудачной авторизации в Instagram API.
        /// </exception>
        /// <param name="userId">Идентификатор пользователя.</param>
        /// <param name="accessToken">Токен доступа.</param>
        /// <param name="cancelationToken">Токен отмены.</param>
        /// <returns>Изображения.</returns>
        public async Task<Response> GetUserImages(string userId, string accessToken, CancellationToken cancelationToken)
        {
            cancelationToken.ThrowIfCancellationRequested();
            var response = await GetUserImages(userId, accessToken, null, null, cancelationToken);

            cancelationToken.ThrowIfCancellationRequested();
            return response;
        }

        public async Task<Response> GetUserImages(string userId, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp, CancellationToken cancelationToken)
        {
            try
            {
                cancelationToken.ThrowIfCancellationRequested();
                var url = InstagramUrlHelper.GetUserUrl(userId, accessToken, minTimeStamp, maxTimeStamp);

                cancelationToken.ThrowIfCancellationRequested();
                var response = await GetInstagramImages(url, cancelationToken);

                cancelationToken.ThrowIfCancellationRequested();
                return response;
            }
            catch (WebException ex)
            {
                // Stop working if request limit reached
                var response = ex.Response as HttpWebResponse;
                if (response != null && (int) response.StatusCode == 429)
                    throw new OverLimitException(
                        "Не удалось получить список изображений потльзователя.\n" +
                        "Клиент пытается отправить слишком много запросов за короткое время, повторите попытку чуть позже.",
                        ex);

                throw new Exception("Не удалось получить список изображений. Ошибка доступа к сети.", ex);
            }
        }

        /// <summary>
        /// Получение изображений по тэгу.
        /// </summary>
        /// <exception cref="AccessDeniedException">
        /// Выбрасывается в случае неудачной авторизации в Instagram API.
        /// </exception>
        /// <param name="tag">Тэг.</param>
        /// <param name="accessToken">Токен доступа к Instagram API.</param>
        /// <param name="cancelationToken">Токен отмены.</param>
        /// <returns>Изображения.</returns>
        public async Task<Response> GetImagesByTag(string tag, string accessToken, CancellationToken cancelationToken)
        {
            cancelationToken.ThrowIfCancellationRequested();
            var response = await GetImagesByTag(tag, accessToken, null, null, cancelationToken);

            cancelationToken.ThrowIfCancellationRequested();
            return response;
        }

        public async Task<Response> GetImagesByTag(string tag, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp, CancellationToken cancelationToken)
        {
            try
            {
                cancelationToken.ThrowIfCancellationRequested();
                var url = InstagramUrlHelper.GetTagUrl(tag, accessToken, minTimeStamp, maxTimeStamp);

                cancelationToken.ThrowIfCancellationRequested();
                var response = await GetInstagramImages(url, cancelationToken);

                cancelationToken.ThrowIfCancellationRequested();
                return response;
            }
            catch (WebException ex)
            {
                // Stop working if request limit reached
                var response = ex.Response as HttpWebResponse;
                if (response != null && (int)response.StatusCode == 429)
                    throw new OverLimitException(
                        "Не удалось получить список изображений по тэгу. Клиент пытается отправить слишком много запросов за короткое время, повторите попытку чуть позже.",
                        ex);

                throw new Exception("Не удалось получить список изображений по тэгу. Ошибка доступа к сети.", ex);
            }
            catch (HttpRequestException ex)
            {
                throw new AccessDeniedException("Не удалось получить список изображений по тэгу. Не удалось авторизоваться в Instagram API.", ex);
            }
        }

        /// <summary>
        /// Подписывает юзера <paramref name="accessToken"/> для которого задан, на юзера кторый зада парамтером <paramref name="userId"/>
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public Task FollowUser(string userId, string accessToken)
        {
            return Task.Run(() =>
            {
                var url = string.Format("https://api.instagram.com/v1/users/{0}/relationship?access_token={1}", userId,
                    accessToken);

                var content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("action", "follow"),
                });

                var httpResponse = _httpClient.PostAsync(url, content);
                httpResponse.Wait();

                var json = httpResponse.Result.Content.ReadAsStringAsync();
                json.Wait();

                var response = JsonConvert.DeserializeObject<RelationshipResponse>(json.Result);

                if (response.Meta.Code == 429)
                    throw new Exception("Было превышено максимальное количество запросов в час.");

                if (response.Meta.Code != 200)
                    throw new Exception("Ошибка подписки " + response.Meta.ErrorMessage);
            });
        }

        public async Task<Response> GetInstagramImages(string url, CancellationToken cancelationToken)
        {
            try
            {
                cancelationToken.ThrowIfCancellationRequested();
                var json = await _httpClient.GetStringAsync(url);

                cancelationToken.ThrowIfCancellationRequested();
                var response = await Task.Run(() => JsonConvert.DeserializeObject<Response>(json), cancelationToken);
                
                cancelationToken.ThrowIfCancellationRequested();
                return response;
            }
            catch (HttpRequestException ex)
            {
                throw new AccessDeniedException("Не удалось получить список изображений. Не удалось авторизоваться в Instagram API.", ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Не удалось получить список изображений.", ex);
            }
        }
    }
}