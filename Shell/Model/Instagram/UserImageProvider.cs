﻿using Boft.Shell.Helpers;

namespace Boft.Shell.Model.Instagram
{
    public class UserImageProvider : InstagramImageProvider
    {
        public UserImageProvider(IInstagramAPIClient client, string accessToken, string userId)
            : base(client, accessToken)
        {
            Url = InstagramUrlHelper.GetUserUrl(userId);
        }
    }
}