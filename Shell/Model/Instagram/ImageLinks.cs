﻿using Newtonsoft.Json;

namespace Boft.Shell.Model.Instagram
{
    internal class ImageLinks
    {
        [JsonProperty("low_resolution")]
        public ImageLink LowResolution { get; set; }

        //[JsonProperty("thumbnail")]
        //public ImageLink Thumbnail { get; set; }

        [JsonProperty("standard_resolution")]
        public ImageLink StandardResolution { get; set; }
    }
}