﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Boft.Shell.Model.Instagram
{
    internal class CaptionInfo
    {
        private static Regex r = new Regex("#\\w+");

        [JsonProperty("text")]
        public string Description { get; set; }

        [JsonProperty("from")]
        public UserInfo From { get; set; }

        public IEnumerable<string> Hashtags
        {
            get
            {
                foreach (Match item in r.Matches(Description))
                {
                    yield return item.Value;
                }
            }
        }
    }
}