﻿using Newtonsoft.Json;
using System;

namespace Boft.Shell.Model.Instagram
{
    public class ImageInfo : IImageInfo
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("images")]
        private ImageLinks _images { get; set; }

        public string PreviewUri
        {
            get
            {
                return _images.LowResolution.Uri;
            }
        }

        public string Uri
        {
            get
            {
                return _images.StandardResolution.Uri;
            }
        }

        [JsonProperty("id")]
        public string ID { get; set; }

        [JsonProperty("location")]
        private LocationInfo _location;

        public string Location
        {
            get
            {
                return _location != null && _location.Description != null ? _location.Description : "";
            }
        }

        [JsonProperty("caption")]
        private CaptionInfo _caption;

        public string Description
        {
            get
            {
                return _caption != null && _caption.Description != null ? _caption.Description : "";
            }
        }

        public IUserInfo User
        {
            get
            {
                return _caption != null && _caption.From != null ? _caption.From : null;
            }
        }

        [JsonProperty("created_time")]
        private long _date;

        public DateTime Date
        {
            get
            {
                return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(_date).ToLocalTime();
            }
        }
    }
}