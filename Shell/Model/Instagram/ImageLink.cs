﻿using Newtonsoft.Json;

namespace Boft.Shell.Model.Instagram
{
    internal class ImageLink
    {
        [JsonProperty("url")]
        public string Uri { get; set; }

        //[JsonProperty("width")]
        //public int Width { get; set; }

        //[JsonProperty("height")]
        //public int Height { get; set; }
    }
}