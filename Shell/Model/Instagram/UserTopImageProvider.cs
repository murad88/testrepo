﻿namespace Boft.Shell.Model.Instagram
{
    internal sealed class UserTopImageProvider : UserImageProvider
    {
        public UserTopImageProvider(IInstagramAPIClient client, string token, string username)
            : base(client, token, username)
        {
        }
    }
}