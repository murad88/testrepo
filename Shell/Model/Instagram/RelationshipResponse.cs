﻿namespace Boft.Shell.Model.Instagram
{
    /// <summary>
    /// Relationship Response
    /// </summary>
    class RelationshipResponse
    {
        /// <summary>
        /// Gets or sets the meta.
        /// </summary>
        /// <value>
        /// The meta.
        /// </value>
        public Meta Meta { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public Relationship Data { get; set; }
    }
}
