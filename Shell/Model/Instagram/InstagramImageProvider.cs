﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Boft.Shell.Helpers;

namespace Boft.Shell.Model.Instagram
{
    /// <summary>
    /// После выполнения метода Start объект данного класса начинает добавлять информацию об изображениях в очередь.
    /// Извлечение элементов из очереди осуществляется вызовом метода GetImages. Событие ImagesRecieved используется для
    /// уведомления о том, что по заданному запросу в Instagram были загружены новые изображения.
    /// </summary>
    public abstract class InstagramImageProvider : IImageProvider
    {
        #region Fields

        /// <summary>
        /// Дата последнего добавленного изображения
        /// </summary>
        private DateTime _lastAddDate;

        /// <summary>
        /// Находится ли провайдер в режиме сканирования новых изображения
        /// </summary>
        private bool _isScanningNewImages;

        protected readonly Timer Timer;

        protected readonly string AccessToken;
        protected readonly IInstagramAPIClient Client;
        protected readonly TimeSpan PeriodSleep = TimeSpan.FromSeconds(1);
        protected readonly CancellationTokenSource Cts = new CancellationTokenSource();
        protected readonly ConcurrentQueue<IImageInfo> Images = new ConcurrentQueue<IImageInfo>();

        private readonly object _lockGetImages = new object();
        private readonly object _lockCallMethod = new object();
        private readonly object _lockLoadImages = new object();

        protected DateTime? StartDate;
        protected DateTime? EndDate;

        protected string Url;
        protected string NextUrl;

        #endregion

        #region Propertyes

        public TimeSpan Period { get; set; }

        public TimeSpan TimeoutGetImages { get; set; }
        public TimeSpan TimeoutLoadImages { get; set; }
        public bool IsAllLoadImages
        {
            get { return NextUrl == null; }
        }
        public ImageProviderState State { get; protected set; }

        #endregion

        #region Constructors

        protected InstagramImageProvider(IInstagramAPIClient client, string accessToken)
        {
            Client = client;
            AccessToken = accessToken;

            Timer = new Timer(sender => LoadImages());

            NextUrl = string.Empty;
            Period = TimeSpan.FromSeconds(5.0);
            TimeoutGetImages = TimeSpan.FromSeconds(60.0);
            TimeoutLoadImages = TimeSpan.FromSeconds(30.0);
        }

        #endregion

        #region Instagram Image Provider

        public async Task<IEnumerable<IImageInfo>> GetImages(int count)
        {
            return await Task.Run(() => GetImagesFromQueue(count));
        }

        public event EventHandler<ImagesRecievedEventArgs> ImagesRecieved;

        public void Start()
        {
            lock (_lockCallMethod)
            {
                if (State != ImageProviderState.NotStarted)
                    throw new Exception("Метод может быть вызван только один раз.");

                if (Timer.Change(TimeSpan.Zero, Period))
                    State = ImageProviderState.Active;
            }
        }

        public void Start(DateTime start, DateTime end)
        {
            if (start > end)
                throw new ArgumentException("start не может быть больще end");

            lock (_lockCallMethod)
            {
                if (State != ImageProviderState.NotStarted)
                    throw new Exception("Метод может быть вызван только один раз.");

                StartDate = start;
                EndDate = end;

                if (Timer.Change(TimeSpan.Zero, Period))
                    State = ImageProviderState.Active;
            }
        }

        public void Stop()
        {
            lock (_lockCallMethod)
            {
                if (State == ImageProviderState.Stopped)
                    return;

                if (State != ImageProviderState.Active && State != ImageProviderState.Paused)
                    throw new Exception(string.Format("Не возможно перейти из состояния {0} в состояние {1}.", State,
                        ImageProviderState.Stopped));

                if (Timer.Change(Timeout.Infinite, Timeout.Infinite))
                {
                    NextUrl = string.Empty;
                    _isScanningNewImages = true;
                    State = ImageProviderState.Stopped;
                }
            }
        }

        public void Pause()
        {
            lock (_lockCallMethod)
            {
                if (State == ImageProviderState.Paused)
                    return;

                if (State != ImageProviderState.Active)
                    throw new Exception(string.Format("Не возможно перейти из состояния {0} в состояние {1}.", State,
                        ImageProviderState.Paused));

                Cts.Cancel();

                if (Timer.Change(Timeout.Infinite, Timeout.Infinite))
                    State = ImageProviderState.Paused;
            }
        }

        public void Resume()
        {
            lock (_lockCallMethod)
            {
                if (State != ImageProviderState.Paused)
                    throw new Exception(string.Format("Не возможно перейти из состояния {0} в состояние {1}.", State,
                       ImageProviderState.Active));

                if (Timer.Change(TimeSpan.Zero, Period))
                    State = ImageProviderState.Active;
            }
        }

        #endregion

        #region Methods

        private void LoadImages()
        {
            lock (_lockLoadImages)
            {
                try
                {
                    Response response;
                    Task<Response> task;

                    if (string.IsNullOrEmpty(NextUrl))
                    {
                        DateTime? startDate;
                        if (IsAllLoadImages)
                        {
                            startDate = _lastAddDate.AddSeconds(1.0);
                            _isScanningNewImages = true;
                        }
                        else 
                            startDate = StartDate;

                        var urlParam = InstagramUrlHelper.AddParametesInQuery(Url, AccessToken, startDate, EndDate);
                        task = Client.GetInstagramImages(urlParam, Cts.Token);

                        response = task.WaitSuccess(TimeoutLoadImages, Cts.Token).Result;

                        if (response.Data.Length > 0)
                            _lastAddDate = response.Data[0].Date;
                    }
                    else
                    {
                        task = Client.GetInstagramImages(NextUrl, Cts.Token);
                        response = task.WaitSuccess(TimeoutLoadImages, Cts.Token).Result;
                    }

                    NextUrl = response.NextUrl;

                    foreach (var userImage in response.Data)
                        Images.Enqueue(userImage);

                    if (_isScanningNewImages && response.Data.Length > 0)
                        OnImagesRecieved(new ImagesRecievedEventArgs(response.Data));

                    Debug.WriteLine("{0} - {1}", response.Data.Length, GetHashCode());
                }
                catch (OperationCanceledException)
                {
                }
            }
        }

        private IEnumerable<IImageInfo> GetImagesFromQueue(int count)
        {
            var waitingTime = TimeSpan.Zero;

            lock (_lockGetImages)
            {
                while (true)
                {
                    // Если в очереде достаточно изображений или все изображения уже были загружены.
                    if (Images.Count > count || IsAllLoadImages)
                        break;

                    if (waitingTime > TimeoutGetImages)
                        throw new TimeoutException();

                    Thread.Sleep(PeriodSleep);
                    waitingTime += PeriodSleep;
                }

                var imageInfos = new List<IImageInfo>();

                for (var i = 0; i < count; i++)
                {
                    IImageInfo dequeueImageInfo;
                    var tryDequeue = Images.TryDequeue(out dequeueImageInfo);

                    if (tryDequeue)
                        imageInfos.Add(dequeueImageInfo);
                    else
                        return imageInfos;
                }

                return imageInfos;
            }
        }

        protected virtual void OnImagesRecieved(ImagesRecievedEventArgs e)
        {
            var handler = ImagesRecieved;
            if (handler != null) handler(this, e);
        }

        #endregion
    }
}