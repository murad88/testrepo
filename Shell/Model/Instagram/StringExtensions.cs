using System;
using System.Globalization;
using System.Net.Http;

namespace Boft.Shell.Model.Instagram
{
    public static class StringExtensions
    {
        public static string UrlEncode(this string input)
        {
            return Uri.EscapeDataString(input);
        }

        public static void AddParameter(this HttpRequestMessage request, string key, IFormattable value)
        {
            if (value != null)
            {
                request.AddParameter(key, value.ToString(null, CultureInfo.InvariantCulture));
            }
        }

        public static void AddParameter(this HttpRequestMessage request, string key, string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return;
            }
            var uriBuilder = new UriBuilder(request.RequestUri);
            var queryToAppend = key.UrlEncode() + "=" + value.UrlEncode();
            uriBuilder.Query = uriBuilder.Query.Length > 1 ? uriBuilder.Query.Substring(1) + "&" + queryToAppend
                : queryToAppend;

            request.RequestUri = uriBuilder.Uri;
        }
    }
}