﻿using Newtonsoft.Json;

namespace Boft.Shell.Model.Instagram
{
    public class Response
    {
        [JsonProperty("data")]
        public ImageInfo[] Data { get; set; }

        [JsonProperty("meta")]
        private Meta _meta;

        public int Code
        {
            get { return _meta.Code; }
        }

        public string ErrorType
        {
            get { return _meta.ErrorType; }
        }

        public string ErrorMessage
        {
            get { return _meta.ErrorMessage; }
        }

        [JsonProperty("pagination")]
        private Pagination _pagination;

        public string NextUrl
        {
            get
            {
                return _pagination != null ? _pagination.NextUrl : null;
            }
        }
    }
}