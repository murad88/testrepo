﻿using Newtonsoft.Json;

namespace Boft.Shell.Model.Instagram
{
    internal class Meta
    {
        [JsonProperty("error_type")]
        public string ErrorType { get; set; }

        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("error_message")]
        public string ErrorMessage { get; set; }
    }
}