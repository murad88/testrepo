﻿namespace Boft.Shell.Model.Instagram
{
    public enum ImageProviderState
    {
        NotStarted,
        Active,
        Paused,
        Stopped
    }
}
