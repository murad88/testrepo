﻿using Newtonsoft.Json;

namespace Boft.Shell.Model.Instagram
{
    internal class LocationInfo
    {
        [JsonProperty("name")]
        public string Description { get; set; }
    }
}