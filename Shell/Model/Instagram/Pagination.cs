﻿using Newtonsoft.Json;

namespace Boft.Shell.Model.Instagram
{
    internal class Pagination
    {
        [JsonProperty("next_url")]
        public string NextUrl { get; set; }

        [JsonProperty("next_max_id")]
        public string NextMaxId { get; set; }
    }
}