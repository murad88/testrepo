﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Boft.Shell.Model.Instagram;

namespace Boft.Shell.Model
{
    public sealed class ImagesRecievedEventArgs : EventArgs
    {
        public IEnumerable<IImageInfo> Images { get; private set; }

        public ImagesRecievedEventArgs(IEnumerable<IImageInfo> images)
        {
            Images = images;
        }
    }

    /// <summary>
    /// Интерефейс классов реализующих доступ к источнику изображений. Объект реализующий класс может находится в четырех
    /// состояниях NotStarted, Active, Paused, Stopped.
    ///
    /// Переходы между состояниями:
    ///
    /// NotStarted -> Active: вызов метода Start;
    /// Active -> Paused: вызов метода Pause;
    /// Paused -> Active: вызов метода Resume;
    /// Active,Paused -> Stopped: вызов метода Stop.
    ///
    /// Остальные переходы запрещены и должны вызвать выброс исключения.
    /// Изменение состояния объекта должно быть потокобезопасным.
    /// </summary>
    internal interface IImageProvider
    {
        /// <summary>
        /// Потокобезопастный метод получения изображений из источника. 
        /// Возвращает изображения начиная, со следующего после возращенного при предудыщем вызове метода.
        /// Возвращает не более count изображений.
        /// В случае если информация о требуемом кол-ве изображений не загружена: ожидает ее загрузки.
        /// Если доступных изображений в источнике меньше чем count возвращает столько сколько есть.
        /// Если все изображения уже были получены из источника то возвращает
        /// пустую коллекцию.
        /// </summary>
        /// <exception cref="OperationCanceledException">
        /// Выбрасывается в случае отмены загрузки изображений.
        /// </exception>
        /// <exception cref="TimeoutException">
        /// Выбрасывается в случае если источник недоступен определенное время.
        /// </exception>
        /// <exception cref="AccessDeniedException">
        /// Выбрасывается в случае или источник недоступен.
        /// </exception>
        /// <param name="count">Запрашиваемое количество изображений.</param>
        /// <returns>Результат получения изображений.</returns>
        Task<IEnumerable<IImageInfo>> GetImages(int count);

        /// <summary>
        /// Уведомляет о получении новых изображений.
        /// </summary>
        event EventHandler<ImagesRecievedEventArgs> ImagesRecieved;

        /// <summary>
        /// Начинает загрузку информации об изображениях источника.
        /// Может быть вызван только один раз, последующие вызовы выбрасывают исключение.
        /// </summary>
        void Start();

        /// <summary>
        /// Начинает загрузку информации об изображениях источника.
        /// Может быть вызван только один раз, последующие вызовы выбрасывают исключение.
        /// </summary>
        /// <param name="start">Начало периода поиска изображений.</param>
        /// <param name="end">Конец периода поиска изображений.</param>
        void Start(DateTime start, DateTime end);

        /// <summary>
        /// Завершает поиск и загрузку изображений из источника.
        /// Кроме непосредственно завершения поиска и загрузки изображений приводит систему в исходное состояние (деавторизация на сервисах, закрытие файлов и т.д.).
        /// Может быть вызван только один раз, дальнейшие вызовы игнорируются.
        /// </summary>
        void Stop();

        /// <summary>
        /// Пауза процесса поиска и загрузки информации из источника изображений.
        /// Если на момент вызова метода производится загрузка изображений, то она отменяется.
        /// Если объект уже находится в состоянии паузы, то метод немедленно возвращает выполнение.
        /// </summary>
        void Pause();

        /// <summary>
        /// Возобновление процесса поиска и загрузки информации из источника изображений (в том числе после сбоя).
        /// </summary>
        void Resume();
    }
}