using System;
using System.Net.Http;

namespace Boft.Shell.Helpers
{
    public static class InstagramUrlHelper
    {
        public const byte DefaulCount = 33;

        public static string GetTagUrl(string tag, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp, byte? count)
        {
            var url = string.Format("https://api.instagram.com/v1/tags/{0}/media/recent?", tag);

            return AddParametesInQuery(url, accessToken, minTimeStamp, maxTimeStamp, count);
        }

        public static string GetTagUrl(string tag, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp)
        {
            return GetTagUrl(tag, accessToken, null, null, DefaulCount);
        }

        public static string GetTagUrl(string tag, string accessToken)
        {
            return GetTagUrl(tag, accessToken, null, null);
        }

        public static string GetTagUrl(string tag)
        {
            return GetTagUrl(tag, null);
        }

        public static string GetUserUrl(string userId, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp, byte? count)
        {
            var url = string.Format("https://api.instagram.com/v1/users/{0}/media/recent?", userId);

            return AddParametesInQuery(url, accessToken, minTimeStamp, maxTimeStamp, count);
        }

        public static string GetUserUrl(string userId, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp)
        {
            return GetUserUrl(userId, accessToken, minTimeStamp, maxTimeStamp, DefaulCount);
        }

        public static string GetUserUrl(string userId, string accessToken)
        {
            return GetUserUrl(userId, accessToken, null, null);
        }

        public static string GetUserUrl(string userId)
        {
            return GetUserUrl(userId, null);
        }

        public static string AddParametesInQuery(string url, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp, byte? count)
        {
            var query = new Uri(url).ParseQueryString();

            if (!string.IsNullOrEmpty(accessToken))
                query["access_token"] = accessToken;

            if (minTimeStamp.HasValue)
                query["min_timestamp"] = minTimeStamp.ToUnixTimeStamp();

            if (maxTimeStamp.HasValue)
                query["max_timestamp"] = maxTimeStamp.ToUnixTimeStamp();

            if (count.HasValue)
                query["count"] = count.ToString();

            var uriBuilder = new UriBuilder(url)
            {
                Port = -1,
                Query = query.ToString()
            };

            return uriBuilder.ToString();
        }

        public static string AddParametesInQuery(string url, string accessToken, DateTime? minTimeStamp, DateTime? maxTimeStamp)
        {
            return AddParametesInQuery(url, accessToken, minTimeStamp, maxTimeStamp, DefaulCount);
        }

        public static string AddParametesInQuery(string url, DateTime? minTimeStamp, DateTime? maxTimeStamp)
        {
            return AddParametesInQuery(url, null, minTimeStamp, maxTimeStamp);
        }
    }
}