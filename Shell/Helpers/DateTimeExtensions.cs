using System;
using System.Globalization;

namespace Boft.Shell.Helpers
{
    public static class DateTimeExtensions
    {
        public static string ToUnixTimeStamp(this DateTime? dateTime)
        {
            return dateTime == null
                ? string.Empty
                : (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).Value.TotalSeconds.ToString(CultureInfo.InvariantCulture);
        }
    }
}