﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Boft.Shell.Helpers
{
    public static class TaskExtensions
    {
        /// <summary>
        /// Создает задачу, подавляющую исключения в процессе выполнения исходной задачи.
        /// </summary>
        /// <typeparam name="T">Тип возвращаемого значения.</typeparam>
        /// <param name="task">Выполняемая задача.</param>
        /// <param name="token">Токен отмены.</param>
        /// <param name="except">Список не подавляемых исключений.</param>
        /// <returns>Задача, подавляющая исключения в процессе выполнения исходной задачи.</returns>
        public static async Task<T> SuppressExceptions<T>(this Task<T> task, CancellationToken token, params Type[] except)
        {
            while (true)
            {
                token.ThrowIfCancellationRequested();

                try
                {
                    var result = await task.ConfigureAwait(false);
                    token.ThrowIfCancellationRequested();
                    return result;
                }
                catch (OperationCanceledException)
                {
                    throw;
                }
                catch (Exception ex)
                {
                    if (except.Any(e => ex.GetType() == e || ex.GetType().IsSubclassOf(e)))
                    {
                        throw;
                    }

                    Debug.WriteLine(ex);
                }

                await Task.Delay(100, token);
            }
        }

        /// <summary>
        /// Создает задачу, ожидающую выполнения исходной задачи.
        /// </summary>
        /// <typeparam name="T">Тип возвращаемого значения.</typeparam>
        /// <param name="task">Выполняемая задача.</param>
        /// <param name="timeout">Время ожидания выполнения задачи.</param>
        /// <param name="token">Токен отмены.</param>
        /// <param name="except">Список не подавляемых исключений.</param>
        /// <returns>Задача, ожидающая выполнения исходной задачи.</returns>
        public static async Task<T> WaitSuccess<T>(this Task<T> task, TimeSpan timeout, CancellationToken token, params Type[] except)
        {
            var internalTask = task.SuppressExceptions(token, except);

            if (await Task.WhenAny(internalTask, Task.Delay(timeout, token)) == internalTask)
            {
                // Task completed within timeout.
                // Consider that the task may have faulted or been canceled.
                // We re-await the task so that any exceptions/cancellation is rethrown.

                return await internalTask;
            }

            throw new TimeoutException();
        }
    }
}