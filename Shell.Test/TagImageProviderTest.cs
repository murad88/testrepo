﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Boft.Shell.Model.Instagram;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Shell.Test
{
    [TestClass]
    public class TagImageProviderTest
    {
        private const string Tag = "мама";
        private const string AccessToken = "1664634702.925b436.27984fef6b11403b84c6c3b0a15f3055";

        [TestMethod]
        public void StartTest()
        {
            var tagImageProvider = new TagImageProvider(new InstagramAPIClient(), AccessToken, Tag);
            tagImageProvider.Start();

            Assert.AreEqual(ImageProviderState.Active, tagImageProvider.State);
        }

        [TestMethod]
        public void StopTest()
        {
            var tagImageProvider = new TagImageProvider(new InstagramAPIClient(), AccessToken, Tag);

            tagImageProvider.Start();
            tagImageProvider.Stop();

            Assert.AreEqual(ImageProviderState.Stopped, tagImageProvider.State);
        }

        [TestMethod]
        public void StartDateTimeTest()
        {
            var tagImageProvider = new TagImageProvider(new InstagramAPIClient(), AccessToken, Tag);

            var start = new DateTime();
            var end = new DateTime();

            tagImageProvider.Start(start, end);
            Assert.AreEqual(ImageProviderState.Active, tagImageProvider.State);
        }

        [TestMethod]
        public void StartArgumentExceptionTest()
        {
            var tagImageProvider = new TagImageProvider(new InstagramAPIClient(), AccessToken, Tag);

            var start = new DateTime(1);
            var end = new DateTime();
            var isSuccess = false;

            try
            {
                tagImageProvider.Start(start, end);
            }
            catch (ArgumentException)
            {
                isSuccess = true;
            }

            Assert.IsTrue(isSuccess);
        }

        [TestMethod]
        public void PauseTest()
        {
            var tagImageProvider = new TagImageProvider(new InstagramAPIClient(), AccessToken, Tag);

            tagImageProvider.Start();
            tagImageProvider.Pause();

            Assert.AreEqual(ImageProviderState.Paused, tagImageProvider.State);
        }

        [TestMethod]
        public void ResumeTest()
        {
            var tagImageProvider = new TagImageProvider(new InstagramAPIClient(), AccessToken, Tag);

            tagImageProvider.Start();
            tagImageProvider.Pause();
            tagImageProvider.Resume();

            Assert.AreEqual(ImageProviderState.Active, tagImageProvider.State);
        }

        [TestMethod]
        public async Task GetImagesTest()
        {
            var tagImageProvider = new TagImageProvider(new InstagramAPIClient(), AccessToken, Tag)
            {
                Period = TimeSpan.FromSeconds(5),
                TimeoutGetImages = TimeSpan.FromMinutes(1),
                TimeoutLoadImages = TimeSpan.FromSeconds(10)
            };

            tagImageProvider.Start();
            var images = await tagImageProvider.GetImages(34);
            tagImageProvider.Stop();

            Assert.AreEqual(34, images.Count());
        }

        [TestMethod]
        public async Task GetImagesEmptyTest()
        {
            var tagImageProvider = new TagImageProvider(new InstagramAPIClient(), AccessToken, "ТегКоторыйНеСуществует1234567")
            {
                Period = TimeSpan.FromSeconds(2),
                TimeoutGetImages = TimeSpan.FromMinutes(0.4),
                TimeoutLoadImages = TimeSpan.FromSeconds(10)
            };

            tagImageProvider.Start();
            await tagImageProvider.GetImages(int.MaxValue);
            var imagesEmpty = await tagImageProvider.GetImages(3);
            tagImageProvider.Stop();

            Assert.AreEqual(0, imagesEmpty.Count());
        }
    }
}
