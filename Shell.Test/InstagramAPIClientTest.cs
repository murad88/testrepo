﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Boft.Shell.Model.Instagram;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Shell.Test
{
    [TestClass]
    public class InstagramAPIClientTest
    {
        private const string Tag = "дождь";
        private const string UserId = "1664634702";
        private const string AccessToken = "1664634702.925b436.27984fef6b11403b84c6c3b0a15f3055";

        [TestMethod]
        public async Task GetUserImagesTest()
        {
            var instagramAPIClient = new InstagramAPIClient();

            var respons = await instagramAPIClient.GetUserImages(UserId, AccessToken, new CancellationToken());

            Assert.IsNotNull(respons.Data);
            Assert.AreEqual(200, respons.Code);
        }

        [TestMethod]
        public void GetUserImagesCancellTest()
        {
            var isSuccsess = false;

            try
            {
                var instagramAPIClient = new InstagramAPIClient();

                var cancellationTokenSource = new CancellationTokenSource();
                var task = instagramAPIClient.GetUserImages(UserId, AccessToken, cancellationTokenSource.Token);
                cancellationTokenSource.Cancel();
                task.Wait(cancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                isSuccsess = true;
            }
            

            Assert.IsTrue(isSuccsess);
        }

        [TestMethod]
        public async Task GetUserImagesTimeStampTest()
        {
            var instagramAPIClient = new InstagramAPIClient();

            var min = new DateTime(2015, 1, 27, 1, 6, 24);
            var max = new DateTime(2015, 1, 27, 1, 17, 39);

            var respons = await instagramAPIClient.GetUserImages(UserId, AccessToken, min, max, new CancellationToken());

            Assert.IsNotNull(respons.Data);
            Assert.AreEqual(200, respons.Code);
        }

        [TestMethod]
        public void GetUserImagesTimeStampCancellTest()
        {
            var isSuccsess = false;

            try
            {
                var instagramAPIClient = new InstagramAPIClient();

                var cancellationTokenSource = new CancellationTokenSource();
                var min = new DateTime(2015, 1, 27, 1, 6, 24);
                var max = new DateTime(2015, 1, 27, 1, 17, 39);

                var task = instagramAPIClient.GetUserImages(UserId, AccessToken, min, max, cancellationTokenSource.Token);
                cancellationTokenSource.Cancel();
                task.Wait(cancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                isSuccsess = true;
            }

            Assert.IsTrue(isSuccsess);
        }

        [TestMethod]
        public async Task GetImagesByTagTest()
        {
            var instagramAPIClient = new InstagramAPIClient();

            var respons = await instagramAPIClient.GetImagesByTag(Tag, AccessToken, new CancellationToken());

            Assert.IsNotNull(respons.Data);
            Assert.AreEqual(200, respons.Code);
        }

        [TestMethod]
        public void GetImagesByTagCancellTest()
        {
            var isSuccsess = false;

            try
            {
                var instagramAPIClient = new InstagramAPIClient();

                var cancellationTokenSource = new CancellationTokenSource();
                var task = instagramAPIClient.GetImagesByTag(Tag, AccessToken, cancellationTokenSource.Token);
                cancellationTokenSource.Cancel();
                task.Wait(cancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                isSuccsess = true;
            }

            Assert.IsTrue(isSuccsess);
        }

        [TestMethod]
        public void FollowUserTest()
        {
            var instagramAPIClient = new InstagramAPIClient();

            var followUser = instagramAPIClient.FollowUser("48369", AccessToken);
            followUser.Wait();

            Assert.AreEqual(TaskStatus.RanToCompletion, followUser.Status);
        }
    }
}
