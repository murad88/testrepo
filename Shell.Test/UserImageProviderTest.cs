﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Boft.Shell.Model.Instagram;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Shell.Test
{
    [TestClass]
    public class UserImageProviderTest
    {
        private const string UserId = "1664634702";
        private const string AccessToken = "1664634702.925b436.27984fef6b11403b84c6c3b0a15f3055";

        [TestMethod]
        public void StartTest()
        {
            var userImageProvider = new UserImageProvider(new InstagramAPIClient(), AccessToken, UserId);
            userImageProvider.Start();

            Assert.AreEqual(ImageProviderState.Active, userImageProvider.State);
        }

        [TestMethod]
        public void StopTest()
        {
            var userImageProvider = new UserImageProvider(new InstagramAPIClient(), AccessToken, UserId);

            userImageProvider.Start();
            userImageProvider.Stop();

            Assert.AreEqual(ImageProviderState.Stopped, userImageProvider.State);
        }

        [TestMethod]
        public void StartDateTimeTest()
        {
            var userImageProvider = new UserImageProvider(new InstagramAPIClient(), AccessToken, UserId);

            var start = new DateTime();
            var end = new DateTime();

            userImageProvider.Start(start, end);
            Assert.AreEqual(ImageProviderState.Active, userImageProvider.State);
        }

        [TestMethod]
        public void StartArgumentExceptionTest()
        {
            var userImageProvider = new UserImageProvider(new InstagramAPIClient(), AccessToken, UserId);

            var start = new DateTime(1);
            var end = new DateTime();
            var isSuccess = false;

            try
            {
                userImageProvider.Start(start, end);
            }
            catch (ArgumentException)
            {
                isSuccess = true;
            }

            Assert.IsTrue(isSuccess);
        }

        [TestMethod]
        public void PauseTest()
        {
            var userImageProvider = new UserImageProvider(new InstagramAPIClient(), AccessToken, UserId);
           
            userImageProvider.Start();
            userImageProvider.Pause();

            Assert.AreEqual(ImageProviderState.Paused, userImageProvider.State);
        }

        [TestMethod]
        public void ResumeTest()
        {
            var userImageProvider = new UserImageProvider(new InstagramAPIClient(), AccessToken, UserId);
           
            userImageProvider.Start();
            userImageProvider.Pause();
            userImageProvider.Resume();

            Assert.AreEqual(ImageProviderState.Active, userImageProvider.State);
        }

        [TestMethod]
        public async Task GetImagesTest()
        {
            var userImageProvider = new UserImageProvider(new InstagramAPIClient(), AccessToken, UserId)
            {
                Period = TimeSpan.FromSeconds(5),
                TimeoutGetImages = TimeSpan.FromMinutes(1),
                TimeoutLoadImages = TimeSpan.FromSeconds(10)
            };

            userImageProvider.Start();
            var images = await userImageProvider.GetImages(34);
            userImageProvider.Stop();

            Assert.AreEqual(34, images.Count());
        }

        [TestMethod]
        public async Task GetImagesEmptyTest()
        {
            var userImageProvider = new UserImageProvider(new InstagramAPIClient(), AccessToken, UserId)
            {
                Period = TimeSpan.FromSeconds(5),
                TimeoutGetImages = TimeSpan.FromMinutes(3),
                TimeoutLoadImages = TimeSpan.FromSeconds(30)
            };

            userImageProvider.Start();
            await userImageProvider.GetImages(int.MaxValue);
            var imagesEmpty = await userImageProvider.GetImages(3);
            userImageProvider.Stop();

            Assert.AreEqual(0, imagesEmpty.Count());
        }

        [TestMethod]
        public async Task GetImagesAllTest()
        {
            var userImageProvider = new UserImageProvider(new InstagramAPIClient(), AccessToken, UserId)
            {
                Period = TimeSpan.FromSeconds(5),
                TimeoutGetImages = TimeSpan.FromMinutes(3),
                TimeoutLoadImages = TimeSpan.FromSeconds(10)
            };
            
            userImageProvider.Start();
            var imagesAll = await userImageProvider.GetImages(int.MaxValue);
            userImageProvider.Stop();

            Assert.IsTrue(imagesAll.Count() < int.MaxValue);
        }
    }
}
